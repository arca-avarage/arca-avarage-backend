<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEmployeeRequest;
use App\Models\PaymentDetail;
use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
  public function index()
  {
    return response()->json(Payment::with(['payment_details'])->first());
  }


  public function store(Request $request)
  {
    // Hitung semua total persentase bonus
    $total_bonus_percentage = array_reduce($request["payment_details"], fn ($acc, $payment_detail) => $acc + $payment_detail["bonus_percentage"], 0);

    // Tolak jika total persentase tidak 100%
    if ($total_bonus_percentage !== 100) {
      return response()->json(['message' => "Persentase bonus tidak 100%"], 422);
    }

    $payment = Payment::updateOrCreate(["id" => $request['id']], [
      "date" => date("Y-m-d H:i:s"),
      "amount" => $request['amount']
    ]);
    
    PaymentDetail::where("payment_id", $payment['id'])->forceDelete();

    $payment_details = [];
    foreach ($request['payment_details'] as $each) {

      $bonus_amount = $payment['amount'] * $each['bonus_percentage'] / 100;

      $created_payment_detail = PaymentDetail::create([
        "employee" => $each['employee'],
        "bonus_percentage" => $each['bonus_percentage'],
        "bonus_amount" => $bonus_amount,
        "payment_id" => $payment['id']
      ]);

      array_push($payment_details, $created_payment_detail);
    }

    return $payment;
  }
}
